# Dependencias:
Para executar este programa é necessário apenas as bibliotecas padrões do C++:

iostream

iomanip

fstream

cstdlib

vector

cstdio

Caso não tenha instalado o compilador de C, digite o seguinte comando no terminal:

`sudo apt-get install build-essential`

# Instruções:
Este programa foi desenvolvido para linux, para compilar o programa digite no terminal, na pasta do repositório:

`make`

Para iniciar o programa digite no terminal:

`make run`

A tela inicial do programa é um menu para selecionar entre os modos do programa, basta digitar o número correspondente à opção que se deseja escolher e apertar ENTER para prosseguir.

* Venda: No modo venda, é pedido para se inserir o nome do comprador em questão. Caso o comprador não seja cadastrado, ele é cadastrado automaticamente como cliente comum, e logo em seguida o programa dá a opção de cadastrá-lo como sócio.
Caso o comprador seja cadastrado mas não seja sócio, o programa dará a opção de cadastrá-lo como sócio.
Caso o comprador seja um sócio, o programa prossegue para a escolha dos produtos.
Então prossegue-se para o menu com os produtos a serem escolhidos. Assim como no menu principal, para escolher uma opção digite o número correspondente ao produto que se quer adicionar, e então apertar ENTER para prosseguir.
Para encerrar as escolha dos produtos digite 0 e aperte ENTER.

* Estoque: No modo estoque, abre-se um menu com 4 opções:
Cadastrar novo produto: permite o cadastro de um novo produto.
Alterar quantidade de um produto no estoque: Permite-se escolher um produto dentre os cadastrados, e alterar a quantidade deste produto no estoque.
Listar produtos: Lista todos os produtos cadastrados no sistema.
Voltar ao menu principal: retorna ao menu principal.

* Recomendação: No modo recomendação, é possível obter uma lista personalizada de produtos recomendados para um cliente cadastrado no sistema.Para isto basta inserir um nome cadastrado na primeira tela, e se receberá uma lista de recomendados correspondente.

* Imprime cadastro de clientes: Mostra na tela uma lista com todos os clientes cadastrados.

* Imprime produtos: Mostra na tela uma lista com todos os produtos cadastrados.

* Encerrar: Encerra o programa, gravando todos as alterações realizadas nos cadastros de clientes e produtos.


