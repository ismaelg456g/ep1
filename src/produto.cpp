#include "produto.hpp"

void Produto::set_nome(string nome){
	this->nome = nome;
}
string Produto::get_nome(){
	return nome;
}
void Produto::set_preco(float preco){
	this->preco = preco;
}
float Produto::get_preco(){
	return preco;
}
void Produto::set_categoria_1(string categoria_1){
	this->categoria_1 = categoria_1;
}
string Produto::get_categoria_1(){
	return categoria_1;
}
void Produto::set_categoria_2(string categoria_2){
	this->categoria_2 = categoria_2;
}
string Produto::get_categoria_2(){
	return categoria_2;
}
void Produto::set_categoria_3(string categoria_3){
	this->categoria_3 = categoria_3;
}
string Produto::get_categoria_3(){
	return categoria_3;
}
void Produto::set_qtd(unsigned int qtd){
	this->qtd = qtd;
}
unsigned int Produto::get_qtd(){
	return qtd;
}
void Produto::set_qtd_compra(unsigned int qtd_compra){
	this->qtd_compra = qtd_compra;
}
unsigned int Produto::get_qtd_compra(){
	return qtd_compra;
}
void Produto::imprime(){
	cout << "Nome do produto: " << get_nome() << endl;
	cout << "Preço: R$ " << get_preco() << endl;
	cout << "Categoria 1: " << get_categoria_1() << endl;
	cout << "Categoria 2: " << get_categoria_2() << endl;
	cout << "Categoria 3: " << get_categoria_3() << endl;
	cout << "Quantidade no estoque: " << get_qtd() << endl;
	//cout << "Quantidade a ser comprada: " << get_qtd_compra() << endl;
}

string Produto::string_para_arq(){
	string str_cadastro;

	str_cadastro='&';
	str_cadastro+='\n';
	str_cadastro+=get_nome();
	str_cadastro+='\n';
	str_cadastro+=to_string(get_preco());
	str_cadastro+='\n';
	str_cadastro+=get_categoria_1();
	str_cadastro+='\n';
	str_cadastro+=get_categoria_2();
	str_cadastro+='\n';
	str_cadastro+=get_categoria_3();
	str_cadastro+='\n';
	str_cadastro+=to_string(get_qtd());
	str_cadastro+='\n';

	return str_cadastro;
}

Produto & Produto::operator=(const Produto & pro){
	if(this != &pro){
		this->nome = pro.nome;
		this->preco = pro.preco;
		this->categoria_1 = pro.categoria_1;
		this->categoria_2 = pro.categoria_2;
		this->categoria_3 = pro.categoria_3;
		this->qtd = pro.qtd;
		this->qtd_compra = pro.qtd_compra;
	}
	return *this;
}

Produto::Produto(const Produto & pro){
	*this=pro;
}

void Produto::adiciona_carrinho(unsigned int quantidade){
	this->qtd_compra += quantidade;
}

string Produto::string_preco(){
	string str_preco;
	int i;

	str_preco += to_string(qtd_compra);
	for(i =0; i< 20-str_preco.size(); i++){
		str_preco += " ";
	}
	str_preco += nome;
	for(i =0; i< 55-nome.size(); i++){
		str_preco += " ";
	}
	str_preco += "R$ ";

	return str_preco;
}

float Produto::get_preco_total(){
	return preco*qtd_compra;
}

void Produto::adiciona_estoque(unsigned int valor){
	this->qtd += valor;
}
bool Produto::tem_estoque(){
	if(qtd<qtd_compra)
		return false;
	else
		return true;
}
void Produto::efetua_compra(){
	qtd-=qtd_compra;
	qtd_compra=0;
}
