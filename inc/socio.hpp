#ifndef SOCIO_HPP
#define SOCIO_HPP

#include "cliente.hpp"

class Socio : public Cliente{
private:
	string email;
	string telefone;
public:
	Socio(): email("email@mail.com"), telefone("XXXXX-XXXX"){
		set_is_socio(true);
	};
	Socio(string n,string e, string t): email(e), telefone(t){
		set_nome(n);
		set_is_socio(true);
	};
	Socio(const Socio &);
	Socio & operator=(const Socio &);
	void set_email(string email);
	string get_email();
	void set_telefone(string telefone);
	string get_telefone();
	void imprime();
	void imprime(vector<Produto *> *produtos);
	string string_para_arq();
};

#endif
