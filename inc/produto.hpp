#ifndef PRODUTO_HPP
#define PRODUTO_HPP

#include <iostream>
#include <string>

using namespace std;

class Produto{
private:
	string nome;
	float preco;
	string categoria_1;
	string categoria_2;
	string categoria_3;
	unsigned int qtd;
	unsigned int qtd_compra;
public:
	Produto(): preco(0.0f), nome(""), categoria_1(""), categoria_2(""), categoria_3(""), qtd(0), qtd_compra(0){};
	Produto(string n, float p,string c1, string c2, string c3, unsigned int q): nome(n), preco(p), categoria_1(c1), categoria_2(c2), categoria_3(c3), qtd(q), qtd_compra(q){};
	Produto(const Produto &);
	Produto & operator=(const Produto &);
	void set_nome(string nome);
	string get_nome();
	void set_preco(float preco);
	float get_preco();
	void set_categoria_1(string categoria_1);
	string get_categoria_1();
	void set_categoria_2(string categoria_2);
	string get_categoria_2();
	void set_categoria_3(string categoria_3);
	string get_categoria_3();
	void set_qtd(unsigned int qtd);
	unsigned int get_qtd();
	void set_qtd_compra(unsigned int qtd);
	unsigned int get_qtd_compra();
	void adiciona_carrinho(unsigned int quantidade);
	void imprime();
	string string_para_arq();
	string string_preco();
	float get_preco_total();
	void adiciona_estoque(unsigned int valor);
	bool tem_estoque();
	void efetua_compra();
};

#endif
