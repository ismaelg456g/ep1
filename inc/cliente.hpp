#ifndef CLIENTE_HPP
#define CLIENTE_HPP


#include <string>
#include <iostream>
#include <vector>
#include "produto.hpp"

using namespace std;

class Cliente{
private:
	string nome;
	bool is_socio;
protected:
	vector<int> compras;
public:
	Cliente();
	Cliente(string n): nome(n), is_socio(0){};
	Cliente(const Cliente &);
	~Cliente();
	Cliente & operator=(const Cliente &);
	void set_nome(string nome);
	string get_nome() const;
	void set_is_socio(bool is_socio);
	bool get_is_socio() const;
	virtual void imprime();
	virtual void imprime(vector<Produto *> *produtos);
	virtual string string_para_arq();
	void adiciona_compra(unsigned int qtd_compra, unsigned int pos_compra);
	vector<int> produtos_mais_comprados();
};

#endif
