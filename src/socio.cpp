#include "socio.hpp"

void Socio::set_email(string email){
	this->email = email;
}
string Socio::get_email(){
	return email;
}
void Socio::set_telefone(string telefone){
	this->telefone = telefone;
}
string Socio::get_telefone(){
	return telefone;
}

void Socio::imprime(){
	cout << "Nome: " << get_nome() << endl;
	cout << "E-mail: "<< get_email() << endl;
	cout << "Telefone: "<< get_telefone() << endl;
	cout << "Sócio: ";
	if(get_is_socio()){
		cout << "Sim" << endl;
	}else{
		cout << "Não" << endl;
	}
}

void Socio::imprime(vector<Produto *> *produtos){
	string str_preco;

	cout << "Nome: " << get_nome() << endl;
	cout << "E-mail: "<< get_email() << endl;
	cout << "Telefone: "<< get_telefone() << endl;
	cout << "Sócio: ";
	if(get_is_socio()){
		cout << "Sim" << endl;
	}else{
		cout << "Não" << endl;
	}
	cout << "Lista de produtos comprados:\n";
	cout << "qtd       nome\n";
	for(int i=0; i<compras.size(); i++){
		str_preco = to_string(compras.at(i));
		cout << str_preco;
		for(int j =0; j< 10-str_preco.size(); j++){
			cout << " ";
		}
		cout << produtos->at(i)->get_nome() << endl;
	}
}

string Socio::string_para_arq(){
	string str_cadastro;

	str_cadastro='&';
	str_cadastro+='\n';
	str_cadastro+=get_nome();
	str_cadastro+='\n';
	str_cadastro+=get_email();
	str_cadastro+='\n';
	str_cadastro+=get_telefone();
	str_cadastro+='\n';
	for(int a: compras){
		str_cadastro+= to_string(a);
		str_cadastro+='\n';
	}
	str_cadastro+='&';
	str_cadastro+='\n';

	return str_cadastro;
}

Socio::Socio(const Socio & socio){
	*this=socio;
}
Socio & Socio::operator=(const Socio & socio){
	if(this != &socio){
		set_nome(socio.get_nome());
		set_is_socio(true);
		this->email=socio.email;
		this->telefone=socio.telefone;
	}

	return *this;
}
