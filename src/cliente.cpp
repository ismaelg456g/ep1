#include "cliente.hpp"

Cliente::Cliente(){
	set_nome("");
	set_is_socio(false);

}

Cliente::~Cliente(){}

void Cliente::set_nome(string nome){
	this->nome = nome;
}
string Cliente::get_nome() const{
	return nome;
}

void Cliente::set_is_socio(bool is_socio){
	this->is_socio = is_socio;
}
bool Cliente::get_is_socio() const{
	return is_socio;
}

void Cliente::imprime(){
	cout << "Nome: " << get_nome() << endl;
	cout << "Sócio: ";
	if(get_is_socio()){
		cout << "Sim" << endl;
	}else{
		cout << "Não" << endl;
	}
}

void Cliente::imprime(vector<Produto *> *produtos){
	string str_preco;

	cout << "Nome: " << get_nome() << endl;
	cout << "Sócio: ";
	if(get_is_socio()){
		cout << "Sim" << endl;
	}else{
		cout << "Não" << endl;
	}
	cout << "Lista de produtos comprados:\n";
	cout << "qtd       nome\n";
	for(int i=0; i<compras.size(); i++){
		str_preco = to_string(compras.at(i));
		cout << str_preco;
		for(int j =0; j< 10-str_preco.size(); j++){
			cout << " ";
		}
		cout << produtos->at(i)->get_nome() << endl;
	}
}

string Cliente::string_para_arq(){
	string str_cadastro;

	str_cadastro='$';
	str_cadastro+='\n';
	str_cadastro+=get_nome();
	str_cadastro+='\n';
	for(int a: compras){
		str_cadastro+= to_string(a);
		str_cadastro+='\n';
	}
	str_cadastro+='$';
	str_cadastro+='\n';

	return str_cadastro;
}

Cliente::Cliente(const Cliente & cliente){
	*this=cliente;
}

Cliente & Cliente::operator=(const Cliente & cliente){
	if(this!= &cliente){
		this->nome = cliente.nome;
		this->is_socio = false;
	}
	return *this;
}
void Cliente::adiciona_compra(unsigned int qtd_compra, unsigned int pos_compra){
	while(pos_compra >= compras.size()){
		compras.push_back(0);
	}
	compras[pos_compra]+= qtd_compra;
}

vector<int> Cliente::produtos_mais_comprados(){
	vector<int> prod_comp;
	vector<int> ordem;
	int i, j, pos=0, max;

	for(i=0; i<compras.size(); i++){
		for(j=0; j<prod_comp.size(); j++){
			if(compras.at(i)>prod_comp.at(j))
				break;
		}
		prod_comp.insert(prod_comp.begin()+j, compras.at(i));
		ordem.insert(ordem.begin()+j, i);
	}

	return ordem;
}
