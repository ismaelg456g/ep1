#include <iostream>
#include <iomanip>
#include <fstream>
#include <cstdlib>
#include <vector>
#include <cstdio>
#include "cliente.hpp"
#include "socio.hpp"
#include "produto.hpp"

using namespace std;

//Opções do menu
void modo_venda(vector<Cliente *> *clientes, vector<Produto *> *produtos);
void modo_estoque(vector<Produto *> *produtos);
void modo_recomendacao(vector<Cliente*> *clientes, vector<Produto *> *produtos);
// Manipulações no cadastro
Socio cadastro_socio(string nome);
Produto cadastro_produto(vector<Produto *> *produtos);
int procura_cadastro(string nome, vector<Cliente *> *clientes);
int procura_socio(string nome, vector<Cliente *> *clientes);
int procura_produto(string nome, vector<Produto *> *produtos);
void altera_estoque(vector<Produto *> *produtos);
//Acesso ao arquivo
void carrega_clientes(vector<Cliente *> *clientes);
void carrega_produtos(vector<Produto *> *produtos);
void descarrega_clientes(vector<Cliente *> *clientes);
void descarrega_produtos(vector<Produto *> *produtos);
//Funções de impressão
void imprime_menu();
void imprime_menu_estoque();
void imprime_produtos(vector<Produto *> *produtos);
void imprime_compra(vector<Produto *> *produtos, bool is_socio);
void imprime_lista_de_produtos(vector<Produto *> *produtos);
void imprime_lista_de_clientes(vector<Cliente *> *clientes);
void imprime_lista_de_clientes(vector<Cliente *> *clientes, vector<Produto *> *produtos);
//Validações
int valida_opcao(int no_opcoes);
string valida_email();
string valida_telefone();
char validaSN();
template <typename T1> T1  getInput();
string getString();
string valida_nome();



int main(){

	int opcao=20;
	vector<Cliente *> clientes;
	vector<Produto *> produtos;

	carrega_clientes(&clientes);
	carrega_produtos(&produtos);

	while(opcao){
		imprime_menu();
		opcao = valida_opcao(5);
		switch(opcao){
			case 1:
				//modo_venda
    		modo_venda(&clientes, &produtos);
				break;
			case 2:
				//modo_estoque
				modo_estoque(&produtos);
				break;
			case 3:
				//modo_recomendação
				modo_recomendacao(&clientes, &produtos);
        system("read dummy");
				break;
			case 4:
				imprime_lista_de_clientes(&clientes, &produtos);
        break;
			case 5:
				imprime_lista_de_produtos(&produtos);
        break;
  		case 0:
        break;
			default:
				exit(-1);
		}
	}

	descarrega_clientes(&clientes);
	descarrega_produtos(&produtos);

	return 0;
}
///////////////////////////Opções do Menu Principal///////////////////////////////
void modo_venda(vector<Cliente*> *clientes, vector<Produto *> *produtos){
  string nome;
  char opcao=-1;
  Cliente *comprador;
  int pos=0;
	bool erro = false;
	int i=0;

  if(produtos->size()==0){
  	cout << "Não há produtos para vender!\n\nCadastre um produto no modo estoque para continuar..." << endl;
  	return;
  }

  system("clear");

  cout << "Insira o nome do comprador : ";
  nome = valida_nome();
  pos = procura_socio(nome, clientes);
  if(pos == -1){
		cout << "\n\nO nome digitado não foi encontrado na lista de sócios..." << endl;
		cout << "Deseja cadastrá-lo como sócio agora?(S/N)" << endl;
		opcao = validaSN();
		pos = procura_cadastro(nome, clientes);
		if(opcao=='s' || opcao=='S'){
			if(pos!=-1){
				clientes->erase(clientes->begin()+pos);
			}
			clientes->push_back(new Socio(cadastro_socio(nome)));
			comprador = clientes->back();
		}else{
			if(pos!=-1){
				comprador = clientes->at(pos);
			}else{
				clientes->push_back(new Cliente(nome));
				comprador = clientes->back();
			}
		}
  }else{
  	comprador = clientes->at(pos);
  }

  while(opcao!=0){
		system("clear");
		cout << "Escolha um produto para adicionar ao carrinho, ou digite 0 para finalizar" << endl;
		imprime_produtos(produtos);
		opcao = valida_opcao(produtos->size());
		if(opcao > 0){
			cout << "que quantidade de " << produtos->at(opcao-1)->get_nome() << " você deseja adicionar ao carrinho?\n: ";
			produtos->at(opcao-1)->adiciona_carrinho(getInput<unsigned int>());
		}
  }


  system("clear");
  imprime_compra(produtos, comprador->get_is_socio());
	system("read dummy");
	for(Produto *p: *produtos){
		if(!p->tem_estoque())
			erro = true;
	}

	if(erro){
		system("clear");
		cout << "Não há produtos suficientes no estoque para efetuar esta compra!!"<< endl;
		for(Produto *p: *produtos){
			p->set_qtd_compra(0);
		}
		system("read dummy");
		return;
	}else{
		for(Produto *p: *produtos){
			comprador->adiciona_compra(p->get_qtd_compra(), i);
			p->efetua_compra();
			i++;
		}
		return;
	}
}
void modo_estoque(vector<Produto *> *produtos){
	int opcao;
	Produto novo_produto;
	int i=0;

	while(1){
		imprime_menu_estoque();
		opcao = valida_opcao(3);
		switch(opcao){
			case 1:
				novo_produto = cadastro_produto(produtos);
				for(i=0; i<produtos->size(); i++){
					if(novo_produto.get_nome() < produtos->at(i)->get_nome())
						break;
				}
				produtos->insert(produtos->begin()+i, new Produto(novo_produto));
				system("clear");
				cout << "Produto cadastrado com sucesso!" << endl;
				break;
			case 2:
				altera_estoque(produtos);
				break;
			case 3:
				imprime_lista_de_produtos(produtos);
				break;
			case 0:
				return;
			default:
				throw(1);
		}
	}

}
void modo_recomendacao(vector<Cliente*> *clientes, vector<Produto *> *produtos){
	string nome;
	int numero_cadastro;
	vector<int> categorias_mais_recomendadas;
	int i=0;

	system("clear");
	cout << "Digite um nome do cadastro para receber uma recomendação: " << endl;
	nome = getString();
	numero_cadastro=procura_cadastro(nome, clientes);

	if(numero_cadastro==-1){
		cout << "Nome não encontrado!" << endl;
		return;
	}

	system("clear");
	categorias_mais_recomendadas = clientes->at(numero_cadastro)->produtos_mais_comprados();

	for(int a: categorias_mais_recomendadas){
		for(Produto *p: *produtos){
			if((p->get_categoria_1()==produtos->at(a)->get_categoria_1()
				|| p->get_categoria_2()==produtos->at(a)->get_categoria_1()
			  || p->get_categoria_3()==produtos->at(a)->get_categoria_1())
				&& p->get_qtd_compra()!=1)
			{
				cout << p->get_nome() << endl;
				p->set_qtd_compra(1);
				i++;
				if(i>=10)
					break;
			}
		}
		if(i>=10)
			break;

		if(!produtos->at(a)->get_categoria_2().empty()){
			for(Produto *p: *produtos){
				if((p->get_categoria_1()==produtos->at(a)->get_categoria_2()
					|| p->get_categoria_2()==produtos->at(a)->get_categoria_2()
					|| p->get_categoria_3()==produtos->at(a)->get_categoria_2())
					&& p->get_qtd_compra()!=1)
				{
					cout << p->get_nome() << endl;
					p->set_qtd_compra(1);
					i++;
					if(i>=10)
						break;
				}
			}
		}
		if(i>=10)
			break;

		if(!produtos->at(a)->get_categoria_3().empty()){
			for(Produto *p: *produtos){
				if((p->get_categoria_1()==produtos->at(a)->get_categoria_3()
					|| p->get_categoria_2()==produtos->at(a)->get_categoria_3()
					|| p->get_categoria_3()==produtos->at(a)->get_categoria_3())
				  && p->get_qtd_compra()!=1)
				{
					cout << p->get_nome() << endl;
					p->set_qtd_compra(1);
					i++;
					if(i>=10)
						break;
				}
			}
		}
		if(i>=10)
			break;
	}

	for(Produto *p: *produtos){
		p->set_qtd_compra(0);
	}
}
///////////////////////////Manipulações no cadastro/////////////////////////////////
Socio cadastro_socio(string nome){
	Socio socio_cadastro;

	system("clear");
	// cout << "Digite um nome : ";
	// socio_cadastro.set_nome(getString());
	socio_cadastro.set_nome(nome);
	cout << "Digite um e-mail: ";
	socio_cadastro.set_email(valida_email());
	cout << "Digite um telefone (XXXXX-XXXX): ";
	socio_cadastro.set_telefone(valida_telefone());

	return socio_cadastro;
}
int procura_cadastro(string nome, vector<Cliente *> *clientes){
	int i=0;
	for(Cliente *c : *clientes){
		if(c->get_nome()==nome){
			return i;
		}
		i++;
	}
	return -1;
}
int procura_socio(string nome, vector<Cliente *> *clientes){
	int i=0;
	for(Cliente *c : *clientes){
		if(c->get_nome()==nome && c->get_is_socio()){
			return i;
		}
		i++;
	}
	return -1;
}
int procura_produto(string nome, vector<Produto *> *produtos){
	int i=0;
	for(Produto *c : *produtos){
		if(c->get_nome()==nome){
			return i;
		}
		i++;
	}
	return -1;
}
Produto cadastro_produto(vector<Produto *> *produtos){
	Produto novo_produto;
	char opcao;
	string nome;

	system("clear");
	cout << "Digite o nome do Produto: ";
	nome = valida_nome();
	while(procura_produto(nome, produtos)!=-1){
		cout << "Este nome já está sendo usado!" << endl;
		cout << "Digite outro nome: " << endl;
		nome = valida_nome();
	}
	novo_produto.set_nome(nome);
	cout << "Digite o preço do produto: " ;
	novo_produto.set_preco(getInput<float>());
	cout << "Digite uma categoria para o produto: ";
	novo_produto.set_categoria_1(valida_nome());
	cout << "Deseja cadastrar mais uma categoria?(S/N) : ";
	opcao = validaSN();
	if(opcao == 's' || opcao == 'S'){
		cout << "Digite uma categoria para o produto: ";
		novo_produto.set_categoria_2(valida_nome());
		cout << "Deseja cadastrar mais uma categoria?(S/N) : ";
		opcao = validaSN();
		if(opcao == 's' || opcao =='S'){
			cout << "Digite uma categoria para o produto: ";
			novo_produto.set_categoria_3(valida_nome());
		}
	}


	cout << "Digite o número de produtos no estoque: ";
	novo_produto.set_qtd(getInput<unsigned int>());

	return novo_produto;

}
void altera_estoque(vector<Produto *> *produtos){
	int num_produto;
	int opcao;

	system("clear");
	if(produtos->size()==0){
		cout << "Não há produtos no estoque!\n" << endl;
		system("read dummy");
		return;
	}

	imprime_produtos(produtos);
	cout << "Escolha um produto para alterar no estoque: ";
	num_produto = valida_opcao(produtos->size());
	system("clear");
	cout << "Você deseja: " << endl;
	cout << "1. Adicionar uma quantidade ao valor atual do estoque;" << endl;
	cout << "2. Ou adicionar um novo valor?" << endl;
	opcao = valida_opcao(2);
	if(opcao==1){
		cout << "Insira o valor a ser adicionado: ";
		produtos->at(num_produto-1)->adiciona_estoque(getInput<unsigned int>());
	}else{
		cout << "Insira a nova quantidade deste produto no estoque: ";
		produtos->at(num_produto-1)->set_qtd(getInput<int>());
	}
	cout << "Valor alterado com sucesso!" << endl;
	system("read dummy");
}
/////////////////////////////Acesso ao arquivo////////////////////////////////////
void carrega_clientes(vector<Cliente *> *clientes){
	char buff[101];
	string comp;
	Socio *aux;
	int i=0;

  fstream arq_cadastro("cadastro.txt", fstream::in);
  if(!arq_cadastro.fail()){
	  while(!arq_cadastro.eof()){
	    arq_cadastro.getline(buff, 100);
			comp = buff;
			if(comp == "&"){
				clientes->push_back(new Socio());
				arq_cadastro.getline(buff, 100);
				aux =(Socio *) clientes->back();
				aux->set_nome(buff);
				arq_cadastro.getline(buff, 100);
				aux->set_email(buff);
				arq_cadastro.getline(buff, 100);
				aux->set_telefone(buff);
				arq_cadastro.getline(buff, 100);
				comp = buff;
				while(comp != "&"){
					aux->adiciona_compra(stoi(comp), i);
					arq_cadastro.getline(buff, 100);
					comp = buff;
					i++;
				}
				i=0;
			}else if(comp == "$"){
				clientes->push_back(new Cliente());
				arq_cadastro.getline(buff, 100);
				clientes->back()->set_nome(buff);
				arq_cadastro.getline(buff, 100);
				comp = buff;
				while(comp != "$"){
					clientes->back()->adiciona_compra(stoi(comp), i);
					arq_cadastro.getline(buff, 100);
					comp = buff;
					i++;
				}
				i=0;
			}
	  }
  }

  arq_cadastro.close();
}
void carrega_produtos(vector<Produto *> *produtos){
  char buff[101];
  string comp;
  Produto *aux;
	int i=0;

  fstream arq_cadastro("produto.txt", fstream::in);
  if(!arq_cadastro.fail()){
	  while(!arq_cadastro.eof()){
	    arq_cadastro.getline(buff, 100);
			comp = buff;
			if(comp == "&"){
				arq_cadastro.getline(buff, 100);
				comp = buff;
				for(i=0; i<produtos->size(); i++){
					if(comp < produtos->at(i)->get_nome())
						break;
				}
				produtos->insert(produtos->begin()+i, new Produto());
				aux = produtos->at(i);
				aux->set_nome(buff);
				arq_cadastro.getline(buff, 100);
				aux->set_preco(stof(buff));
				arq_cadastro.getline(buff, 100);
				aux->set_categoria_1(buff);
				arq_cadastro.getline(buff, 100);
				aux->set_categoria_2(buff);
				arq_cadastro.getline(buff, 100);
				aux->set_categoria_3(buff);
				arq_cadastro.getline(buff, 100);
				aux->set_qtd(stof(buff));
			}
	  }
  }

  arq_cadastro.close();
}
void descarrega_clientes(vector<Cliente *> *clientes){
	fstream arq("cadastro.txt", fstream::out | fstream::trunc);

	for(Cliente *c : *clientes){
		arq << c->string_para_arq();
	}

	arq.close();
}
void descarrega_produtos(vector<Produto *> *produtos){
	fstream arq("produto.txt", fstream::out | fstream::trunc);

	for(Produto *p : *produtos){
		arq << p->string_para_arq();
	}

	arq.close();
}
/////////////////////////////Funções de impressão/////////////////////////////q
void imprime_menu(){
	system("clear");
	cout << "Escolha uma opção" << "\n\n";
	cout << "1. Venda" << endl;
	cout << "2. Estoque"  << endl;
	cout << "3. Recomendação" << endl;
	cout << "4. Imprime cadastro de clientes"  << endl;
	cout << "5. Imprime produtos"  << endl;
  cout << "0. Encerrar" << "\n\n" << ": ";
}
void imprime_menu_estoque(){
	system("clear");
	cout << "O que deseja fazer?\n" <<endl;
	cout << "1. Cadastrar novo produto" << endl;
	cout << "2. Alterar quantidade de um produto no estoque" << endl;
	cout << "3. Listar produtos" << endl;
	cout << "0. Voltar ao menu principal" << endl;
	cout << ": ";
}
void imprime_produtos(vector<Produto *> *produtos){
	int i =1;
	for(Produto *p: *produtos){
		cout << i << ": " << p->get_nome() << endl;
		i++;
	}
}
void imprime_compra(vector<Produto *> *produtos, bool is_socio){
	float preco_total=0;
	cout.precision(2);
	cout << "qtd        ";
	cout << "nome                                                   ";
	cout << "preço" << endl;
	for(Produto *p: *produtos){
		if(p->get_qtd_compra()>0){
			cout << p->string_preco();
			cout << setiosflags (ios::fixed) << p->get_preco_total() << endl;
			preco_total+=p->get_preco_total();
		}
	}
	cout << "Total:                                                            ";
	cout << setiosflags (ios::fixed) << "R$ " << preco_total << endl;
	if(is_socio){
		cout << "-Desconto de 15%" << endl;
		preco_total*=0.85;
		cout << "Total com desconto:                                               ";
		cout << setiosflags (ios::fixed) << "R$ " << preco_total << endl;
	}

}
void imprime_lista_de_produtos(vector<Produto *> *produtos){
	int i=0;

	system("clear");
	cout << "----------------------" << endl;
	for(Produto *c: *produtos){
		i++;
		c->imprime();
		cout << "----------------------" << endl;
	}
	if(i==0){
		cout << "Não há nenhum produto cadastrado!" << endl;
		i=0;
	}
	system("read dummy");
}
void imprime_lista_de_clientes(vector<Cliente *> *clientes){
	int i=0;

	system("clear");
	cout << "----------------------" << endl;
	for(Cliente *c: *clientes){
		i++;
		c->imprime();
		cout << "----------------------" << endl;
	}
	if(i==0){
		cout << "Não há nenhum cliente cadastrado!" << endl;
		i=0;
	}
	system("read dummy");
}
void imprime_lista_de_clientes(vector<Cliente *> *clientes, vector<Produto *> *produtos){
	int i=0;

	system("clear");
	cout << "----------------------" << endl;
	for(Cliente *c: *clientes){
		i++;
		c->imprime(produtos);
		cout << "----------------------" << endl;
	}
	if(i==0){
		cout << "Não há nenhum cliente cadastrado!" << endl;
		i=0;
	}
	system("read dummy");
}
////////////////////////////////Validações///////////////////////////////////////////
int valida_opcao(int no_opcoes){
	int opcao = getInput<int>();

	while(opcao < 0 || opcao > no_opcoes){
		cout << "\n\nOpção inválida!!! digite um número entre 0 e ";
		cout << no_opcoes << " :";
		opcao = getInput<int>();
	}
	return opcao;
}
string getString(){
  string valor;
  getline(cin, valor);
  return valor;
}
string valida_email(){
	string email;

	email = valida_nome();
	while(email.find('@') == -1 && email.find('.') == -1){
		cerr << "\n\n\nE-mail inválido! Digite um e-mail válido!\n";
		cerr << "Ex.: email@email.com\n: ";
		email = valida_nome();
	}
	return email;
}
string valida_telefone(){
	string telefone;
	bool erro=false;

	telefone = valida_nome();
	for(char a: telefone){
		if(a!='-' && (a<'0' || a>'9'))
			erro=true;
	}
	while((telefone.size()>10 && telefone.size()<9)
	      || (telefone[4]!='-' && telefone[5]!='-')
				|| (telefone[4]=='-' && telefone[5]=='-')
				|| erro){

		erro = false;
		cerr << "\n\n\nTelefone inválido! Digite um Telefone válido!\n";
		cerr << "Ex.: 99999-0000 ou 3333-0000\n: ";
		telefone = valida_nome();
		for(char a: telefone){
			if(a!='-' && a<'0' && a>'9')
				erro=true;
		}
	}
	return telefone;
}
char validaSN(){
	char opcao = cin.get();
	cin.ignore(32767, '\n');

	while(opcao != 's' && opcao != 'S' && opcao!= 'n' && opcao!= 'N'){
		cout << "Opção inválida!\n\nDigite 'S' ou 'N':" << endl;
		opcao = cin.get();
		cin.ignore(32767, '\n');
	}
	return opcao;
}
template <typename T1>
T1  getInput(){
  T1 valor;

  while(1){
    cin >> valor;
    if(cin.fail()){
      cin.clear();
      cin.ignore(32767, '\n');
      cout << "Entrada inválida!\n\n\n";
      cout << "Digite novamente!\n";
    }else{
      cin.ignore(32767, '\n');
      return valor;
    }
  }
}
string valida_nome(){
	string nome;
	nome = getString();
	while(nome == "$" || nome == "&"){
		cout << "Entrada inválida!\n\n";
		cout << "Este é um símbolo reservado, digite outra entrada: " ;
		nome = getString();
	}
	return nome;
}
